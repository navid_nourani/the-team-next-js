import { Org } from "@prisma/client";
import "next-auth";

type UserType = User & { userRole: "user" };
type OrgUserType = Org & { userRole: "admin" };

export type IUserType = UserType | OrgUserType;

declare module "next-auth" {
  interface Session {
    user: IUserType;
  }
}
