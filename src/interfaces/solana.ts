export interface IWalletProvider {
  connect: (e: any) => any;
  disconnect: (e: any) => any;
  request: (e: any) => any;
  isPhantom: boolean;
  publicKey: any;
  isConnected: boolean;
  on: (value: string, func: () => void) => void;
}
