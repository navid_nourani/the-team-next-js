export interface IWallet {
  name: string;
  image: string;
  address: string;
}

export interface ISocial {
  name: ISocialTypes;
  id?: string;
  link?: string;
}

export type ISocialTypes =
  | "twitter"
  | "facebook"
  | "instagram"
  | "ticktok"
  | "youtube"
  | "telegram"
  | "discord";
