import { Org, OrgSocial, UserSocial } from "@prisma/client";

export type IOrgDetails = Pick<Org, "name" | "logo"> & { socials: OrgSocial[] };

export type IUserSocial = Omit<UserSocial, "id" | "userId">;
