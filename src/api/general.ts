import { PrismaClient } from "@prisma/client";

export const initialOrganization = async () => {
  const prisma = new PrismaClient();
  const orgDetail = await prisma.org.findFirst({
    select: { name: true, logo: true, socials: true },
  });
  prisma.$disconnect();
  return orgDetail;
};
