import { User } from "@prisma/client";
import { IUpdateUserResponse } from "../../pages/api/users/updateUser";

export const updateUserApi = async (user: User): Promise<boolean> => {
  const res = await fetch(`/api/users/updateUser`, {
    body: JSON.stringify(user),
    method: "POST",
  });
  const data: IUpdateUserResponse = await res.json();
  return data.success;
};
