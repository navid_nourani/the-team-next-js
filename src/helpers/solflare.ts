import { IWalletProvider } from "../interfaces/solana";

export const getSolflareProvider = async () => {
  if (!window) {
    return;
  }

  const typeLessWindow = window as any;
  if ("solflare" in window) {
    await typeLessWindow.solflare.connect(); // opens wallet to connect to
    const provider = typeLessWindow.solflare;
    if (provider.isSolflare) {
      return provider as IWalletProvider;
    }
  }
  return null;
};
