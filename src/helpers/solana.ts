import { IWalletProvider } from "../interfaces/solana";

export const getSolanaProvider = async () => {
  if (!window) {
    return;
  }

  const typeLessWindow = window as any;
  if ("solana" in window) {
    await typeLessWindow.solana.connect(); // opens wallet to connect to

    const provider = typeLessWindow.solana;
    if (provider.isPhantom) {
      return provider as IWalletProvider;
    }
  }
  return null;
};
