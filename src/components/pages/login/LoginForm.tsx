import styled from "@emotion/styled";
import {
  Button,
  Checkbox,
  FormControlLabel,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useUpdateAtom } from "jotai/utils";
import { signIn } from "next-auth/react";
import { useSnackbar } from "notistack";
import React, { FormEvent, FunctionComponent, useState } from "react";
import { isLoginAtom } from "../../../store/pages/login";
import LoadingButton from "../../shared/LoadingButton";

const LoginForm: FunctionComponent = () => {
  const { enqueueSnackbar } = useSnackbar();
  const setIsLogin = useUpdateAtom(isLoginAtom);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();
    loginHandler();
  };

  const loginHandler = async () => {
    setLoading(true);
    const res: any = await signIn("credentials", {
      username,
      password,
      redirect: false,
    });
    if (res?.ok) {
      // enqueueSnackbar("Login Successful", { variant: "success" });
    } else {
      enqueueSnackbar("Login Failed", { variant: "error" });
    }
    setLoading(false);
  };

  return (
    <Form onSubmit={onSubmit}>
      <StyledTextField
        id="username"
        label="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
      <StyledTextField
        id="password"
        type="password"
        label="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <FormControlLabel
        componentsProps={{
          typography: { variant: "caption" },
        }}
        sx={{ marginBottom: "2rem" }}
        label="Remember me"
        control={<Checkbox />}
      />
      <Stack direction="row" alignItems={"center"} marginBottom="1rem">
        <Typography variant="caption">
          {"Don't have an account yet?"}
        </Typography>{" "}
        &nbsp;
        <Button sx={{ fontSize: "0.75rem" }} onClick={() => setIsLogin(false)}>
          {"Signup"}
        </Button>
      </Stack>
      <LoadingButton
        loading={loading}
        variant="contained"
        color="primary"
        type="submit"
      >
        Login
      </LoadingButton>
    </Form>
  );
};

const StyledTextField = styled(TextField)`
  margin-bottom: 1.5rem;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export default LoginForm;
