import { CheckCircle } from "@mui/icons-material";
import { CircularProgress, InputAdornment, TextField } from "@mui/material";
import { User } from "@prisma/client";
import React, { FunctionComponent } from "react";
import { Control } from "react-hook-form";
import { IUsernameStatus } from "../SignupForm";

type IProps = {
  disabled: boolean;
  control: Control<
    User & {
      passwordConfirmation: string;
    },
    any
  >;
  usernameErrorMessage?: string;
  usernameStatus: IUsernameStatus;
};

const Username: FunctionComponent<IProps> = ({
  control,
  disabled,
  usernameStatus,
  usernameErrorMessage,
}) => {
  return (
    <TextField
      disabled={disabled}
      sx={{ marginBottom: "1.5rem" }}
      error={!!usernameErrorMessage || usernameStatus === "exists"}
      helperText={`${usernameErrorMessage ?? ""}
          ${usernameStatus === "exists" ? " Username already exists" : ""}`}
      label="Username"
      {...control.register("username")}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            {usernameStatus === "loading" && (
              <CircularProgress
                sx={{
                  width: "1.5rem !important",
                  height: "1.5rem !important",
                }}
              />
            )}
            {usernameStatus === "ok" && (
              <CheckCircle
                sx={{
                  width: "1.5rem !important",
                  height: "1.5rem !important",
                }}
                color="success"
              />
            )}
          </InputAdornment>
        ),
      }}
    />
  );
};

export default Username;
