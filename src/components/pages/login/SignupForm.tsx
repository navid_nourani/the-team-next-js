import styled from "@emotion/styled";
import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Stack, TextField, Typography } from "@mui/material";
import { User } from "@prisma/client";
import axios from "axios";
import bycrypt from "bcryptjs";
import { useUpdateAtom } from "jotai/utils";
import { useSnackbar } from "notistack";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { CheckUsenameExistsResponseType } from "../../../../pages/api/users/checkUsernameExists";
import { isLoginAtom } from "../../../store/pages/login";
import LoadingButton from "../../shared/LoadingButton";
import Username from "./signupForm/Username";
import { signupSchema } from "./signupForm/yupValidator";

export type IUsernameStatus = "ok" | "exists" | "loading" | "unknown";

const SignupForm: FunctionComponent = () => {
  const setIsLogin = useUpdateAtom(isLoginAtom);
  const { enqueueSnackbar } = useSnackbar();
  const timeoutRef = useRef<NodeJS.Timeout>();
  const [usernameStatus, setUsernameStatus] =
    useState<IUsernameStatus>("unknown");
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<User & { passwordConfirmation: string }>({
    resolver: yupResolver(signupSchema),
  });

  const onSubmit = async (data: User & { passwordConfirmation: string }) => {
    const { passwordConfirmation, password, ...requestBody } = data;
    const hashedSalt = bycrypt.hashSync("I am Strong sault", 10);
    const hashedPass = bycrypt.hashSync(data.password, hashedSalt);

    setIsSubmitting(true);
    try {
      const response = await axios.post("/api/users/signup", {
        ...requestBody,
        password: hashedPass,
      });
      if (response.status === 200) {
        setIsLogin(true);
        enqueueSnackbar("Signup Successful", { variant: "success" });
      } else {
        enqueueSnackbar("Try again, an error eccured", { variant: "error" });
      }
    } catch (e: any) {
      enqueueSnackbar("Try again, an error eccured", { variant: "error" });
    } finally {
      setIsSubmitting(false);
    }
  };

  const checkUsernameExists = async (username: string) => {
    if (username.length < 4) {
      return;
    }
    setUsernameStatus("loading");
    const response = await axios.get<CheckUsenameExistsResponseType>(
      "/api/users/checkUsernameExists",
      { params: { username } }
    );
    if (response.data.success) {
      setUsernameStatus(response.data.data?.exists ? "exists" : "ok");
    }
  };

  useEffect(() => {
    setUsernameStatus("unknown");
    if (timeoutRef) {
      clearTimeout(timeoutRef.current as any);
      timeoutRef.current = setTimeout(
        () => checkUsernameExists(watch("username")),
        700
      );
    }
  }, [watch("username")]);

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <StyledTextField
        disabled={isSubmitting}
        label="Name"
        {...control.register("name")}
      />
      <Username
        disabled={isSubmitting}
        control={control}
        usernameStatus={usernameStatus}
        usernameErrorMessage={errors.username?.message}
      />
      <StyledTextField
        type="password"
        error={!!errors.password}
        helperText={errors.password?.message}
        label="password"
        {...control.register("password")}
        disabled={isSubmitting}
      />
      <StyledTextField
        disabled={isSubmitting}
        type="password"
        error={!!errors.passwordConfirmation}
        helperText={errors.passwordConfirmation?.message}
        label="Password confirmation"
        {...control.register("passwordConfirmation")}
      />
      <Stack direction="row" alignItems={"center"} marginBottom="0.75rem">
        <Typography variant="caption">{"Do you have an account?"}</Typography>{" "}
        &nbsp;
        <Button sx={{ fontSize: "0.75rem" }} onClick={() => setIsLogin(true)}>
          {"Login"}
        </Button>
      </Stack>
      <LoadingButton
        disabled={isSubmitting}
        type="submit"
        variant="contained"
        color="primary"
        loading={isSubmitting}
      >
        Signup
      </LoadingButton>
    </Form>
  );
};

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const StyledTextField = styled(TextField)`
  margin-bottom: 1.5rem;
`;

export default SignupForm;
