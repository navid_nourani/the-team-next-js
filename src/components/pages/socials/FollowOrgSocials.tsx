import { CheckCircle } from "@mui/icons-material";
import { Button, Grid, Stack, Typography } from "@mui/material";
import { OrgSocial } from "@prisma/client";
import { useAtom, useAtomValue } from "jotai";
import React, { FunctionComponent } from "react";
import { IUserSocial } from "../../../interfaces/dbTypes";
import { orgDetailAtom } from "../../../store/org";
import { orgFollowedAccountsByUserAtom } from "../../../store/pages/socials";
import SocialIcon from "../../shared/SocialDetails/SocialIcon";

const FollowOrgSocials: FunctionComponent = () => {
  const [followedAccountsByUser, setFollowedAccountByuser] = useAtom(
    orgFollowedAccountsByUserAtom
  );
  const orgDetails = useAtomValue(orgDetailAtom);

  const handleSubmit = (social: OrgSocial) => {
    window.open(social.url, "_blank");
    if (
      !followedAccountsByUser.find((account) => account.name === social.name) &&
      social.url
    ) {
      setFollowedAccountByuser([
        ...followedAccountsByUser,
        { ...social, followedOrgAccount: true } as IUserSocial,
      ]);
    }
  };

  return (
    <Stack alignItems="center">
      <Typography marginBottom="48px" variant="h2">
        Follow us on social media
      </Typography>
      <Grid container columns={8} justifyContent="center" spacing="24px">
        {orgDetails.socials.map((social) => {
          const isFollowed = followedAccountsByUser.find(
            (account) => account.name === social.name
          );
          return (
            <Grid item key={social.name}>
              <Button
                sx={{ color: isFollowed ? "primary.light" : "primary.dark" }}
                onClick={() => handleSubmit(social)}
              >
                <Stack direction="column">
                  {/* <AttachMoney sx={{ opacity: isFollowed ? 0 : 1 }} /> */}
                  <SocialIcon color="inherit" size={4} name={social.name} />
                  {isFollowed && (
                    <CheckCircle
                      sx={{
                        borderRadius: "50%",
                        padding: "0",
                        margin: "0",
                        border: "1px solid white",
                        color: "primary.main",
                        position: "absolute",
                        right: 0,
                      }}
                    />
                  )}
                </Stack>
              </Button>
            </Grid>
          );
        })}
      </Grid>
    </Stack>
  );
};

export default FollowOrgSocials;
