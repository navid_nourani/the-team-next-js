import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import { useAtom } from "jotai";
import React, { FunctionComponent, useEffect } from "react";
import { getSolanaProvider } from "../../../helpers/solana";
import { getSolflareProvider } from "../../../helpers/solflare";
import {
  userSolanaWalletAtom,
  userSolflareWalletAtom,
} from "../../../store/pages/socials";
import Wallet from "./UserWallets/Wallet";

const UserWallets: FunctionComponent = () => {
  const [solana, setSolana] = useAtom(userSolanaWalletAtom);
  const [solflare, setSolflare] = useAtom(userSolflareWalletAtom);

  const initalSolanaProvider = async () => {
    const solona = await getSolanaProvider();
    const solflare = await getSolflareProvider();
    setSolana(solona);
    setSolflare(solflare);
  };

  useEffect(() => {
    if (window) {
      initalSolanaProvider();
    }
  }, []);

  return (
    <HorizontalStack>
      <Typography variant="body1">Your wallets: </Typography>
      <Wallet
        name="Phantom"
        address={solana?.publicKey.toString()}
        image={"/images/phantom logo.png"}
        installLink="https://www.phantom.app/"
      />
      <Wallet
        name="Solflare"
        address={solflare?.publicKey.toString()}
        image={"/images/solflare logo.png"}
        installLink="https://www.solflare.com/"
      />
    </HorizontalStack>
  );
};

const HorizontalStack = styled(Stack)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
  & > *:not(:last-child) {
    margin-right: 16px;
  }
`;

export default UserWallets;
