import styled from "@emotion/styled";
import { Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Image from "next/image";
import React, { FunctionComponent } from "react";
import { IWallet } from "../../../../interfaces/social";

type IProps = Omit<IWallet, "address"> & {
  address?: string;
  installLink: string;
};

const Wallet: FunctionComponent<IProps> = ({
  name,
  address,
  image,
  installLink,
}) => {
  if (!address && !installLink) {
    return null;
  }
  return (
    <Stack direction="row" alignItems="center">
      <ImageContainer>
        <Image src={image} alt={name} layout="fill" />
      </ImageContainer>
      {address && (
        <Typography>
          {address.slice(0, 4)}...
          {address.slice(address.length - 4, address.length)}
        </Typography>
      )}
      {!address && (
        <Typography>
          Install on &nbsp;
          <StyledLink target="_blank" rel="noreferrer" href={installLink}>
            {name}
          </StyledLink>
        </Typography>
      )}
    </Stack>
  );
};

const StyledLink = styled("a")`
  color: #048aaa;
  text-decoration: underline;
`;

const ImageContainer = styled(Box)`
  width: 40px;
  height: 40px;
  position: relative;
  margin-right: 12px;
`;

export default Wallet;
