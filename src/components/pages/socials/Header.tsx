import { Container } from "@mui/material";
import { Box } from "@mui/system";
import Image from "next/image";
import React, { FunctionComponent } from "react";

const Header: FunctionComponent = () => {
  return (
    <Box width="100vw" sx={{ height: "90px", flexShrink: 0 }}>
      <Container
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: "18px",
          height: "100%",
        }}
      >
        <Box sx={{ width: 147, height: "100%", position: "relative" }}>
          <Image
            src="/images/nooft social logo.png"
            alt="nooft social"
            layout="fill"
            objectFit="contain"
          />
        </Box>
      </Container>
    </Box>
  );
};

export default Header;
