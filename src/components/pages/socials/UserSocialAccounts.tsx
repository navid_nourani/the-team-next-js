import styled from "@emotion/styled";
import { Button, Grid, LinearProgress, Stack, Typography } from "@mui/material";
import { Social, UserSocial } from "@prisma/client";
import axios from "axios";
import { useAtomValue } from "jotai";
import { useUpdateAtom } from "jotai/utils";
import React, { FunctionComponent, useEffect, useState } from "react";
import useSWR from "swr";
import { IGetUserSocialsResponse } from "../../../../pages/api/users/getUserSocials";
import { orgDetailAtom } from "../../../store/org";
import { currentStepAtom } from "../../../store/pages/socials";
import SocialDetails from "../../shared/SocialDetails";

const socialsFetcher = async (url: string) => {
  return axios.get(url).then((res) => res.data);
};

const UserSocialAccounts: FunctionComponent = () => {
  const [userSocials, setUserSocials] = useState<UserSocial[]>([]);
  const orgDetail = useAtomValue(orgDetailAtom);
  const [submitSocialsProgress, setSubmitSocialsProgress] = useState(0);
  const goToSextStep = useUpdateAtom(currentStepAtom);
  const { data: userSubmitedSocials, error: userSubmitedSocialsError } =
    useSWR<IGetUserSocialsResponse>(
      `/api/users/getUserSocials`,
      socialsFetcher
    );

  // Initialize user Socials
  useEffect(() => {
    let userSocialsObject: UserSocial[] = userSocials;
    if (userSocials.length < 1) {
      userSocialsObject = orgDetail?.socials?.map((orgSocial) => {
        return {
          name: orgSocial.name,
          followedOrgAccount: false,
          socialId: "",
        } as UserSocial;
      });
    }
    if (userSubmitedSocials?.success) {
      userSocialsObject = userSocials?.map((orgSocial) => {
        const filledSocial = userSubmitedSocials.data.socials.find(
          (x) => x.name === orgSocial.name
        );
        if (filledSocial) {
          return filledSocial;
        }
        console.log("Filling social", filledSocial);
        return {
          name: orgSocial.name,
          followedOrgAccount: false,
          socialId: "",
        } as UserSocial;
      });
    }
    console.log("userSocialsObject", userSubmitedSocials);
    setUserSocials(userSocialsObject);
  }, [orgDetail?.socials, userSubmitedSocials]);

  // Calculating Progress
  useEffect(() => {
    if (!userSocials) {
      return;
    }
    const validSocialsCount = userSocials?.filter(
      (social) => social.socialId.length > 2
    ).length;
    setSubmitSocialsProgress(
      (validSocialsCount / orgDetail?.socials.length ?? 1) * 100
    );
  }, [orgDetail?.socials?.length, userSocials]);

  const handleSubmit = (socialName: Social, socialId: string) => {
    const manipulatedSocials = userSocials.map((social) => {
      if (social.name === socialName) {
        return { ...social, socialId };
      }
      return social;
    });
    setUserSocials(manipulatedSocials);
  };

  return (
    <VeritcalStack>
      <Typography marginBottom="48px" variant="h3">
        Your Social Accounts:
      </Typography>
      <LinearProgress
        sx={{
          width: "100%",
          height: "50px",
          borderRadius: "50px",
          marginBottom: "48px",
        }}
        variant="determinate"
        value={submitSocialsProgress}
      />

      <Grid
        container
        spacing={1}
        columns={12}
        justifyContent="center"
        rowGap="24px"
        marginBottom="48px"
      >
        {userSocials?.map((social) => {
          return (
            <Grid item key={social.name}>
              <SocialDetails
                social={social}
                onSubmit={(value) => handleSubmit(social.name, value)}
              />
            </Grid>
          );
        })}
      </Grid>

      <Button
        variant="contained"
        onClick={() => goToSextStep("complete")}
        sx={{
          fontFamily: "sigmarone",
          width: "100%",
          maxWidth: "400px",
          borderRadius: "50px",
        }}
      >
        Next Step
      </Button>
    </VeritcalStack>
  );
};

const VeritcalStack = styled(Stack)`
  /* flex-direction: row; */
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
  & > *:not(:last-child) {
  }
`;

export default UserSocialAccounts;
