import styled from "@emotion/styled";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { Button, Stack, TextField } from "@mui/material";
import { useSession } from "next-auth/react";
import { useSnackbar } from "notistack";
import React, { FunctionComponent, useEffect, useMemo } from "react";
import Web3 from "web3";
import { updateUserApi } from "../../../api/updateUserApi";

const AddUserWallet: FunctionComponent = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { data, status } = useSession();

  const [walletAddress, setWalletAddress] = React.useState("");
  const [isValidAddress, setIsValidAddress] = React.useState(false);
  const web3 = new Web3(process.env.NEXT_PUBLIC_WEB3_PROVIDER ?? "");

  const walletAddressValidation = (value: string) => {
    const isValid = web3.utils.isAddress(value);
    setIsValidAddress(!!isValid);
    console.log("isValid", isValid);
  };

  useEffect(() => {
    walletAddressValidation(walletAddress);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [walletAddress]);

  const inputEndAdorment = useMemo(() => {
    if (isValidAddress) {
      return <CheckCircleIcon color="success" />;
    }
    return <CancelIcon color="error" />;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isValidAddress]);

  const submitWallet = async () => {
    if (!isValidAddress) {
      enqueueSnackbar("Enter a valid address", { variant: "warning" });
      return;
    }
    const result = await updateUserApi({ ...data?.user, walletAddress });
    if (result) {
      enqueueSnackbar("Wallet address updated", { variant: "success" });
    } else {
      enqueueSnackbar("Something went wrong", { variant: "error" });
    }
  };

  return (
    <Stack>
      <HorizontalStack>
        <TextField
          sx={{ width: "31rem" }}
          label="Wallet address"
          value={walletAddress}
          onChange={(e) => setWalletAddress(e.target.value)}
          InputProps={{
            endAdornment: walletAddress.length >= 1 && inputEndAdorment,
          }}
        />
        <Button disabled={!isValidAddress} onClick={submitWallet}>
          Submit
        </Button>
      </HorizontalStack>
    </Stack>
  );
};

const HorizontalStack = styled(Stack)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
  & > *:not(:last-child) {
    margin-right: 16px;
  }
`;

export default AddUserWallet;
