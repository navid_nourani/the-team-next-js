import { Box, Button, ButtonProps, CircularProgress } from "@mui/material";
import React, { FunctionComponent } from "react";

type Props = ButtonProps & { loading?: boolean };

const LoadingButton: FunctionComponent<Props> = ({ loading, ...props }) => {
  return (
    <Button
      {...props}
      disabled={loading}
      startIcon={
        loading ? <Box sx={{ width: "1.5rem", height: "1.5rem" }} /> : undefined
      }
      endIcon={
        loading ? (
          <CircularProgress
            color="primary"
            sx={{ width: "1.5rem !important", height: "1.5rem !important" }}
          />
        ) : undefined
      }
    />
  );
};

export default LoadingButton;
