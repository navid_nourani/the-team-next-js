import styled from "@emotion/styled";
import { Instagram, Telegram, Twitter, YouTube } from "@mui/icons-material";
import Facebook from "@mui/icons-material/Facebook";
import { Box } from "@mui/material";
import { Social } from "@prisma/client";
import React, { FunctionComponent } from "react";
import Discord from "/public/images/discord.svg";
import Ticktok from "/public/images/ticktok.svg";

type IProps = { name: Social; color?: string; size?: number };

const SocialIcon: FunctionComponent<IProps> = ({
  name,
  color = "white",
  size = 1.5,
}) => {
  switch (name.toLowerCase()) {
    case "facebook":
      return (
        <Facebook sx={{ color, width: `${size}rem`, height: `${size}rem` }} />
      );
    case "twitter":
      return (
        <Twitter sx={{ color, width: `${size}rem`, height: `${size}rem` }} />
      );
    case "instagram":
      return (
        <Instagram sx={{ color, width: `${size}rem`, height: `${size}rem` }} />
      );
    case "telegram":
      return (
        <Telegram sx={{ color, width: `${size}rem`, height: `${size}rem` }} />
      );
    case "youtube":
      return (
        <YouTube sx={{ color, width: `${size}rem`, height: `${size}rem` }} />
      );
    case "discord":
      return (
        <LogoContainer color={color} size={size}>
          <Discord />
        </LogoContainer>
      );
    case "ticktok":
      return (
        <LogoContainer color={color} size={size}>
          <Ticktok />
        </LogoContainer>
      );
    default:
      return null;
  }
};

const LogoContainer = styled(Box)((props: any) => ({
  color: props.color ?? "white",
  position: "relative",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  "& > *": {
    padding: `${((props.size * 16) / 8).toFixed(0)}px`,
    height: `${props.size}rem`,
    width: `${props.size}rem`,
    color: props.color,
  },
}));

export default SocialIcon;
