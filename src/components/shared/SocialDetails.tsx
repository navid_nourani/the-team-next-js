import { InputAdornment, Stack, TextField } from "@mui/material";
import { UserSocial } from "@prisma/client";
import React, { FunctionComponent } from "react";
import SocialIcon from "./SocialDetails/SocialIcon";

type IProps = { social: UserSocial; onSubmit?: (value: string) => void };

const SocialDetails: FunctionComponent<IProps> = ({ social, onSubmit }) => {
  return (
    <Stack direction="row" alignItems="center">
      <TextField
        value={social.socialId}
        onChange={(e) => !!onSubmit && onSubmit(e.target.value)}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SocialIcon name={social.name} />
            </InputAdornment>
          ),
        }}
        label={social.name}
      />
    </Stack>
  );
};

export default SocialDetails;
