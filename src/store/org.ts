import { atom } from "jotai";
import { IOrgDetails } from "../interfaces/dbTypes";

const defaultOrg: IOrgDetails = {
  name: "",
  logo: "",
  socials: [],
};

export const orgDetailAtom = atom<IOrgDetails>(defaultOrg);
