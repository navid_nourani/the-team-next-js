import { atom } from "jotai";
import { atomWithDefault, atomWithStorage } from "jotai/utils";
import { IUserSocial } from "../../interfaces/dbTypes";
import { IWalletProvider } from "./../../interfaces/solana";

export const currentStepAtom = atomWithDefault<
  "wallet" | "user_socials" | "complete"
>((get) => {
  if (!get(userSolanaWalletAtom) && !get(userSolflareWalletAtom)) {
    return "wallet";
  }
  return "user_socials";
});

export const orgFollowedAccountsByUserAtom = atomWithStorage<IUserSocial[]>(
  "@orgFollowedAccountsByUser",
  []
);

//! Remove ba khiale rahat
// export const useSocialsProgressAtom = atom((get) => {
//   const socials = get(userSocialAccountsAtom);
//   const progress = socials.reduce(socialProgressReducer, 0);

//   return { value: progress, from: socials.length };
// });

// const socialProgressReducer = (progressAcc: number, social: ISocial) => {
//   if (social.id) {
//     return progressAcc + 1;
//   }
//   return progressAcc;
// };

export const userSolanaWalletAtom = atom<IWalletProvider | undefined | null>(
  undefined
);

export const userSolflareWalletAtom = atom<IWalletProvider | undefined | null>(
  undefined
);
