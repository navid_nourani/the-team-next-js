import { ThemeOptions } from "@mui/material";

export const themeOptions: ThemeOptions = {
  palette: {
    mode: "dark",
    primary: {
      main: "#01f7f7",
    },
    secondary: {
      main: "#ffffff",
    },
  },
  typography: {
    h1: {
      fontFamily: "sigmarone",
      fontSize: "3rem",
    },
    h2: {
      fontFamily: "sigmarone",
      fontSize: "2.25rem",
    },
    h3: {
      fontFamily: "sigmarone",
      fontSize: "1.5rem",
    },
    h4: {
      fontFamily: "sigmarone",
      fontSize: "1rem",
    },
    h5: {
      fontFamily: "sigmarone",
      fontSize: "0.875rem",
    },
    h6: {
      fontFamily: "sigmarone",
      fontSize: "0.75rem",
    },
    fontFamily: '"ms-sans-serif", "mmrtext", "Cerebri Sans SemiBold", "serif"',
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1450,
    },
  },
};
