# Nooft Socials Platfom

A Next.js project to help users to follow Nooft Project social medias and get Nooft token as a gift.

Used technologies:

- Next.js
- React.js
- Jotai
- Material-UI
- Web3.js

Vercel address: [Click Me!](https://the-team-next-js.vercel.app/ "Click Me!")
Landing page design: [Adobe XD](http://https://xd.adobe.com/view/69dfd909-6d6f-4586-820b-91802940af6e-3c8c/ "Adobe XD")
