import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function main() {
  const org = await prisma.org.findFirst();
  if (!org) {
    const newOrg = await prisma.org.create({
      data: {
        username: "admin",
        password:
          "$2a$10$surv4R8aDmytnX83sh0p0unZT/BExZw1wQ.lJMF4Zm7Qn3g.rhZFC",
        name: "Nooft Project",
        logo: "/images/nooft social logo.png",
      },
    });
    console.log("Initial Organization created", { newOrg });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
