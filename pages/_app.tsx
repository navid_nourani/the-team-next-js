import { ThemeProvider } from "@emotion/react";
import { createTheme, StyledEngineProvider } from "@mui/material";
import { PrismaClient } from "@prisma/client";
import { useHydrateAtoms } from "jotai/utils";
import { getSession, SessionProvider } from "next-auth/react";
import App, { AppProps } from "next/app";
import Head from "next/head";
import { SnackbarProvider } from "notistack";
import { orgDetailAtom } from "../src/store/org";
import { themeOptions } from "../src/theme";
import "../styles/globals.css";

const theme = createTheme(themeOptions);

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  console.log("MyApp", pageProps);
  //@ts-ignore
  useHydrateAtoms([[orgDetailAtom, pageProps.orgDetail]]);

  return (
    <>
      <Head>
        <title>The team</title>
        <link
          rel="preload"
          href="/fonts/mmrtext.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/sigmarone.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/ms-sans-serif.ttf"
          as="font"
          crossOrigin=""
        />
      </Head>
      <SessionProvider session={session}>
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={theme}>
            <SnackbarProvider maxSnack={3}>
              <Component {...pageProps} />
            </SnackbarProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </SessionProvider>
    </>
  );
}

MyApp.getInitialProps = async (context: any) => {
  const superInitialData = await App.getInitialProps(context);
  const session = context && (await getSession(context));

  let orgDetail: object | null = {};
  if (process.env.NODE_ENV === "development") {
    orgDetail = {
      name: "Nooft Project",
      logo: "/images/nooft social logo.png",
      socials: [
        {
          id: 1,
          name: "Facebook",
          socialId: "nooftproject",
          url: "https://www.facebook.com/nooftproject",
          orgId: 2,
        },
        {
          id: 2,
          name: "Telegram",
          socialId: "nooftproject",
          url: "https://t.me/nooftproject",
          orgId: 2,
        },
        {
          id: 3,
          name: "Discord",
          socialId: "gwkKEAQvCh",
          url: "https://discord.gg/gwkKEAQvCh",
          orgId: 2,
        },
        {
          id: 4,
          name: "Youtube",
          socialId: "nooftproject",
          url: "https://www.youtube.com/c/nooftproject",
          orgId: 2,
        },
        {
          id: 5,
          name: "Twitter",
          socialId: "NooftProject",
          url: "https://twitter.com/NooftProject",
          orgId: 2,
        },
        {
          id: 6,
          name: "Ticktok",
          socialId: "@nooft.io",
          url: "https://www.tiktok.com/@nooft.io",
          orgId: 2,
        },
        {
          id: 7,
          name: "Instagram",
          socialId: "nooftproject",
          url: "https://www.instagram.com/nooftproject/",
          orgId: 2,
        },
      ],
    };
  } else {
    const prisma = new PrismaClient();
    orgDetail = await prisma.org.findFirst({
      select: { name: true, logo: true, socials: true },
    });
    prisma.$disconnect();
  }

  superInitialData.pageProps = {
    ...superInitialData.pageProps,
    orgDetail,
    session,
  };
  return superInitialData;
};

export default MyApp;
