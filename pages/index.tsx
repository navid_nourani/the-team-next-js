import styled from "@emotion/styled";
import { Stack } from "@mui/material";
import type { NextPage } from "next";
import Link from "next/link";
const Home: NextPage = () => {
  return (
    <Stack>
      <Link href="/landing" passHref>
        <StyledLink>Landing</StyledLink>
      </Link>
      <Link href="/socials" passHref>
        <StyledLink>Socials</StyledLink>
      </Link>
      <Link href="/login" passHref>
        <StyledLink>Login</StyledLink>
      </Link>
      <Link href="/web3" passHref>
        <StyledLink>web 3</StyledLink>
      </Link>
    </Stack>
  );
};

const StyledLink = styled("a")`
  font-family: mmrtext;
  font-size: 1.5rem;
`;

export const getStaticProps = async () => {
  return {
    props: {},
  };
};

export default Home;
