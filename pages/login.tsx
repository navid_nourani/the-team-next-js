import styled from "@emotion/styled";
import { Container, Stack } from "@mui/material";
import { useAtomValue } from "jotai";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { FunctionComponent, useEffect } from "react";
import LoginComponent from "../src/components/pages/login/LoginForm";
import SignupForm from "../src/components/pages/login/SignupForm";
import Header from "../src/components/pages/socials/Header";
import { isLoginAtom } from "../src/store/pages/login";

type Props = {};

const Login: FunctionComponent = (props: Props) => {
  const isLogin = useAtomValue(isLoginAtom);
  const { status, data } = useSession();
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  useEffect(() => {
    if (status === "authenticated") {
      enqueueSnackbar(`Wellcome ${data.user?.name}`, { variant: "success" });
      router.replace("/socials");
    }
  }, [data?.user?.name, enqueueSnackbar, router, status]);

  return (
    <Stack height="100%" marginX={"1rem"}>
      <Header />
      <Container
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          width: "350px",
        }}
      >
        <LoginFormContainer>
          {isLogin ? <LoginComponent /> : <SignupForm />}
        </LoginFormContainer>
      </Container>
    </Stack>
  );
};

const LoginFormContainer = styled(Stack)`
  align-self: center;
  background-color: #333333ee;
  border-radius: 1.5rem;
  padding: 2rem;
`;

export const getStaticProps = async () => {
  return {
    props: {},
  };
};

export default Login;
