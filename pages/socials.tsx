import { Container } from "@mui/material";
import { useAtomValue } from "jotai";
import type { NextPage } from "next";
import { useForm } from "react-hook-form";
import AddUserWallet from "../src/components/pages/socials/AddUserWallet";
import FollowOrgSocials from "../src/components/pages/socials/FollowOrgSocials";
import Header from "../src/components/pages/socials/Header";
import UserSocialAccounts from "../src/components/pages/socials/UserSocialAccounts";
import UserWallets from "../src/components/pages/socials/UserWallets";
import { currentStepAtom } from "../src/store/pages/socials";

const Socials: NextPage = () => {
  const {
    control,
    formState: { errors },
  } = useForm();
  const currentStep = useAtomValue(currentStepAtom);
  return (
    <>
      <Header />
      <Container>
        <UserWallets />
        <AddUserWallet />
        {currentStep !== "complete" && <UserSocialAccounts />}
        {currentStep === "complete" && <FollowOrgSocials />}
      </Container>
    </>
  );
};

export const getStaticProps = async () => {
  return {
    props: {},
  };
};

export default Socials;
