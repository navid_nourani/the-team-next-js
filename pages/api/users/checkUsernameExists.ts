import { PrismaClient, User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";

export type IApiResponse<T> =
  | {
      success: false;
      message?: string;
    }
  | {
      success: true;
      message?: string;
      data: T;
    };

export type CheckUsenameExistsResponseType = IApiResponse<{ exists: boolean }>;

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<CheckUsenameExistsResponseType>
) {
  const username = req.query.username as string;
  const prisma = new PrismaClient();
  try {
    if (username) {
      const exists = await prisma.user.findFirst({
        where: { username },
      });
      res.status(200).json({
        success: true,
        message: "successfully added",
        data: { exists: !!exists },
      });
    } else {
      res.status(400).json({
        success: false,
        message: "Send all required field",
      });
    }
  } catch (e) {
    console.log("add sell api error", e);
    res.status(500).json({
      success: false,
      message: "Backend problem" + e,
    });
  } finally {
    prisma.$disconnect();
  }
  return;
}
