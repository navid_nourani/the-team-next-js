import { PrismaClient, UserSocial } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import { IApiResponse } from "./checkUsernameExists";

export type IGetUserSocialsResponse = IApiResponse<{ socials: UserSocial[] }>;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IGetUserSocialsResponse>
) {
  const session = await getSession({ req });
  const prisma = new PrismaClient();
  try {
    if (session?.user) {
      const userSocials = await prisma.userSocial.findMany({
        where: { userId: session.user.id },
      });
      res.status(200).json({
        success: true,
        data: { socials: userSocials },
      });
    } else {
      res.status(400).json({
        success: false,
        message: "Login...",
      });
    }
  } catch (e) {
    res.status(500).json({
      success: false,
      message: "Backend problem" + e,
    });
  } finally {
    prisma.$disconnect();
  }
  return;
}
