import { PrismaClient, User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import { IApiResponse } from "./checkUsernameExists";

export type IAddUserRequest = User;

export type IUpdateUserResponse = IApiResponse<boolean>;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IUpdateUserResponse>
) {
  const session = await getSession({ req });
  const prisma = new PrismaClient();
  try {
    if (req.method === "POST") {
      const user = JSON.parse(req.body) as IAddUserRequest;
      if (session?.user && user.id === session.user.id) {
        await prisma.user.update({
          data: user,
          where: { id: user.id },
        });
        res.status(200).json({
          success: true,
          message: "successfully added",
          data: true,
        });
      } else {
        res.status(400).json({
          success: false,
          message: "Login, send user data and Edit just YOUR account",
        });
      }
    } else {
      res.status(405).json({
        success: false,
        message: "Just POST method is allowed",
      });
    }
  } catch (e) {
    console.log("add sell api error", e);
    res.status(500).json({
      success: false,
      message: "Backend problem" + e,
    });
  } finally {
    prisma.$disconnect();
  }
  return;
}
