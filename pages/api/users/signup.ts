import { PrismaClient, User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";

export type IAddSellResponse = {
  success: boolean;
  message?: string;
};

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IAddSellResponse>
) {
  const user: IAddUserRequest = req.body;
  const prisma = new PrismaClient();
  try {
    if (user && user.username && user.password) {
      await prisma.user.create({
        data: user,
      });
      res.status(200).json({
        success: true,
        message: "successfully added",
      });
    } else {
      res.status(400).json({
        success: false,
        message: "Send all required field",
      });
    }
  } catch (e) {
    res.status(500).json({
      success: false,
      message: "Backend problem" + e,
    });
  } finally {
    prisma.$disconnect();
  }
  return;
}
