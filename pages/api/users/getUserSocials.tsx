import { PrismaClient, User, UserSocial } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import { IApiResponse } from "./checkUsernameExists";

export type IGetUserSocialsResponse = IApiResponse<{
  userId: number;
  socials: UserSocial[];
}>;

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IGetUserSocialsResponse>
) {
  const session = (await getSession({ req })) as any;
  const prisma = new PrismaClient();

  try {
    if (!session) {
      throw new Error("You dont have proffer previllages");
    }

    const userRole = (session.user as any).userRole;
    if (userRole === "admin") {
      const userId = req.query.userId as string;
      if (!userId) {
        throw new Error("Please send userId");
      }
      const userSocials = await prisma.userSocial.findMany({
        where: { userId: Number(userId) },
      });

      res.status(200).json({
        success: true,
        data: { userId: Number(userId), socials: userSocials },
      });
    } else {
      const userSocials = await prisma.userSocial.findMany({
        where: { userId: session.user.id },
      });

      res.status(200).json({
        success: true,
        data: { userId: session.user.id, socials: userSocials },
      });
    }
  } catch (e) {
    console.log("add sell api error", e);
    res.status(500).json({
      success: false,
      message: e.message,
    });
  } finally {
    prisma.$disconnect();
  }
  return;
}
