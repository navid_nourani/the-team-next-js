import { User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import Web3 from "web3";
import { IApiResponse } from "../users/checkUsernameExists";

export type IBalanceResponse = IApiResponse<{
  address: string;
  balance: string;
}>;

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IBalanceResponse>
) {
  try {
    const walletAddress = req.query.walletAddress as string;
    const web3 = new Web3(process.env.NEXT_PUBLIC_WEB3_PROVIDER ?? "");
    const weiBalance = await web3.eth.getBalance(walletAddress ?? "");

    const balance = web3.utils.fromWei(weiBalance, "ether");
    res.status(200).json({
      success: true,
      data: { address: walletAddress, balance: balance },
    });
  } catch (error: any) {
    console.error(error);
    res.status(500).json({ success: false, message: error.message });
  }
  return;
}
