import { User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import Web3 from "web3";
import { IApiResponse } from "../users/checkUsernameExists";
var Tx = require("ethereumjs-tx").Transaction;

export type IBalanceResponse = IApiResponse<{
  transactionHash: any;
}>;

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IBalanceResponse>
) {
  try {
    const userWalletAddress = req.query.walletAddress as string;

    const companyWalletAddress = process.env.NEXT_PUBLIC_ETHEREUM_ADDRESS ?? "";
    const companyPvKey = Buffer.from(
      process.env.NEXT_PUBLIC_PRIVATE_KEY ?? "",
      "hex"
    );

    const web3 = new Web3(process.env.NEXT_PUBLIC_WEB3_PROVIDER ?? "");

    const txCount = await web3.eth.getTransactionCount(companyWalletAddress);

    // Build the transacion
    const txObject = {
      to: userWalletAddress,
      value: web3.utils.toHex(
        web3.utils.toWei(process.env.NEXT_PUBLIC_GIFT_AMOUNT ?? "", "ether")
      ),
      gasLimit: web3.utils.toHex(21000),
      gasPrice: web3.utils.toHex(web3.utils.toWei("10", "gwei")),
      nonce: web3.utils.toHex(txCount),
    };

    // Sign the transaction
    const tx = new Tx(txObject, { chain: "rinkeby" });
    tx.sign(companyPvKey);
    const serilizedTX = tx.serialize();
    const raw = "0x" + serilizedTX.toString("hex");

    res.status(200).json({
      success: true,
      data: { transactionHash: "" },
    });

    // Broadcast the tansaction
    const a = await web3.eth.sendSignedTransaction(raw);
    console.log("txHash", a);
  } catch (error: any) {
    console.error(error);
    res.status(500).json({ success: false, message: error.message });
  }
  return;
}
