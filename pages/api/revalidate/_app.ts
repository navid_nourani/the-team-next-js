import { User } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { IApiResponse } from "../users/checkUsernameExists";

export type IBalanceResponse = IApiResponse<undefined>;

export type IAddUserRequest = User;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IBalanceResponse>
) {
  const security = req.query.security as String;
  try {
    if (security === process.env.NEXT_PUBLIC_REVALIDATE_SECURITY) {
      res.unstable_revalidate("/");
      res.status(200).json({
        success: true,
        message: "revalidated",
        data: undefined,
      });
    } else {
      res.status(500).json({
        success: false,
        message: "Invalid security token",
      });
    }
  } catch (error: any) {
    console.error(error);
    res.status(500).json({ success: false, message: error.message });
  }
  return;
}
