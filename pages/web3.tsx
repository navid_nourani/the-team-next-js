import { Button, Stack, TextField } from "@mui/material";
import React, { useState } from "react";
import FullScreenHeightLayout from "../src/components/shared/FullScreenHeightLayout";
import { IBalanceResponse } from "./api/web3/balance";

const Web3 = () => {
  const [walletAddress, setWalletAddress] = useState("");
  const [walletBalance, setWalletBalance] = useState("");
  const getBalance = async () => {
    if (!walletAddress) {
      return;
    }

    const res = await fetch(
      `/api/web3/sendGift?walletAddress=${walletAddress}`
    );
    const responceData: IBalanceResponse = await res.json();
    if (responceData.success) {
      setWalletBalance(responceData.data.balance);
    }
  };
  return (
    <FullScreenHeightLayout>
      <Stack direction="column">
        <h1>Web3 TEST</h1>
        <TextField
          value={walletAddress}
          onChange={(e) => setWalletAddress(e.target.value)}
        />
        <TextField disabled value={walletBalance} />
        <Button onClick={getBalance}>Get Balance</Button>
      </Stack>
    </FullScreenHeightLayout>
  );
};

export const getStaticProps = async () => {
  return {
    props: {},
  };
};

export default Web3;
